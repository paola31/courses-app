import React, { useState, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';
import Input from '../../common/Input/Input';
import Button from '../../common/Button/button';
import { useParams, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { addAuthorThunk } from '../../store/authors/thunk';
import { addCourseThunk, updateCourseThunk } from '../../store/courses/thunk';
import { fetchCourses } from '../../store/courses/actionCreators';
import './CourseForm.css';

function CourseForm() {
	const { courseId } = useParams();
	const isUpdateMode = courseId !== undefined;
	const createButtonText = isUpdateMode ? 'Update Course' : 'Create Course';
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [duration, setDuration] = useState('');
	const [courseAuthors, setCourseAuthors] = useState([]);
	const [newAuthorName, setNewAuthorName] = useState('');
	const [formattedDuration, setFormattedDuration] = useState('');
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const authors = useSelector((state) => state.authors);
	const courses = useSelector((state) => state.courses);
	useEffect(() => {
		if (isUpdateMode) {
			console.log('Is updated mode ');
			const course = courses.find((course) => course.id === courseId);
			if (course) {
				setTitle(course.title);
				setDescription(course.description);
				setDuration(course.duration);
				setCourseAuthors(
					course.authors.map((authorId) =>
						authors.find((author) => author.id === authorId)
					)
				);
			}
		}
	}, [courseId, isUpdateMode, courses, authors]);

	useEffect(() => {
		const formatDuration = (duration) => {
			const hours = Math.floor(duration / 60);
			const minutes = duration % 60;
			return `Duration: ${hours}:${minutes.toString().padStart(2, '0')} hours`;
		};

		setFormattedDuration(formatDuration(duration));
	}, [duration]);

	const handleCreateAuthor = () => {
		if (newAuthorName.trim().length < 2) {
			alert('Author name must be at least 2 characters long.');
			return;
		}
		const newAuthor = { id: uuidv4(), name: newAuthorName.trim() };
		dispatch(addAuthorThunk(newAuthor));
		setNewAuthorName('');
	};

	const handleAddAuthor = (authorId) => {
		const authorToAdd = authors.find((author) => author.id === authorId);
		if (!authorToAdd) return;

		setCourseAuthors([...courseAuthors, authorToAdd]);
	};

	const handleDeleteAuthor = (authorId) => {
		setCourseAuthors(courseAuthors.filter((author) => author.id !== authorId));
	};

	const renderAuthorsList = () => {
		return authors.map((author) => (
			<div key={author.id} className='author'>
				<span>{author.name}</span>
				<Button
					buttonText='Add Author'
					onClick={() => handleAddAuthor(author.id)}
				/>
			</div>
		));
	};

	const renderCourseAuthorsList = () => {
		return courseAuthors.map((author) => (
			<div key={author.id} className='author'>
				<span>{author.name}</span>
				<Button
					buttonText='Delete Author'
					onClick={() => handleDeleteAuthor(author.id)}
				/>
			</div>
		));
	};

	const handleSubmit = async () => {
		if (
			title === '' ||
			description === '' ||
			duration === '' ||
			courseAuthors.length === 0
		) {
			alert('All fields are required.');
			return;
		}

		const newCourse = {
			id: isUpdateMode ? courseId : uuidv4(),
			title,
			description,
			creationDate: isUpdateMode
				? courses.find((course) => course.id === courseId).creationDate
				: new Date().toLocaleDateString('en-GB'),
			duration: parseInt(duration),
			authors: courseAuthors.map((author) => author.id),
		};

		if (isUpdateMode) {
			dispatch(updateCourseThunk(newCourse.id, newCourse));
			dispatch(fetchCourses());
		} else {
			await dispatch(addCourseThunk(newCourse));
			dispatch(fetchCourses());
		}
		navigate('/courses');
	};

	const handleCancel = () => {
		navigate('/courses');
	};
	return (
		<div className='create-course'>
			<div className='course-top-row'>
				<Input
					placeholder='Course name'
					value={title}
					onChange={(e) => setTitle(e.target.value)}
					id='courseTitle'
					labelText='Title'
				/>
				<Button buttonText={createButtonText} onClick={handleSubmit} />
				<Button buttonText='Cancel' onClick={handleCancel} />
			</div>
			<label htmlFor={description}>Description</label>
			<textarea
				className='course-description'
				placeholder='Description'
				value={description}
				id='description'
				onChange={(e) => setDescription(e.target.value)}
			/>

			<div className='course-authors-duration'>
				<div className='left-side'>
					<div className='bottom-gap'>
						<h3>Add Author</h3>
						<Input
							placeholder='Author name'
							value={newAuthorName}
							onChange={(e) => setNewAuthorName(e.target.value)}
							id='authorName'
							labelText='Author Name'
						/>
					</div>
					<div className='bottom-gap'>
						<Button buttonText='Create Author' onClick={handleCreateAuthor} />
					</div>
					<div className='bottom-gap'>
						<Input
							type='number'
							placeholder='Duration (minutes)'
							value={duration}
							onChange={(e) => setDuration(e.target.value)}
							id='duration'
							labelText='Duration'
						/>
					</div>
					<div className='formatted-duration'>{formattedDuration}</div>
				</div>

				<div className='right-side'>
					<h3>Authors</h3>
					<div className='authors-list'>{renderAuthorsList()}</div>
					<h3>Course Authors</h3>
					<div className='course-authors-list'>{renderCourseAuthorsList()}</div>
				</div>
			</div>
		</div>
	);
}

export default CourseForm;
