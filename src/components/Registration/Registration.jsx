import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import Input from '../../common/Input/Input';
import Button from '../../common/Button/button';
import './Registration.css';

function Registration() {
	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const history = useNavigate();

	const handleSubmit = async (event) => {
		event.preventDefault();

		const newUser = {
			name,
			password,
			email,
		};

		try {
			const response = await axios.post(
				'http://localhost:4000/register',
				newUser,
				{
					headers: {
						'Content-Type': 'application/json',
					},
				}
			);

			console.log('Response:', response);

			history('/login');
		} catch (error) {
			console.error('Error registering user:', error);
			console.error('Error response:', error.response);
		}
	};

	return (
		<div className='registration'>
			<h2>Registration</h2>
			<form onSubmit={handleSubmit}>
				<label htmlFor='name'>Name:</label>
				<Input
					id='name'
					type='text'
					placeholder='Name'
					value={name}
					onChange={(e) => setName(e.target.value)}
				/>

				<label htmlFor='email'>Email:</label>
				<Input
					id='email'
					type='email'
					placeholder='Email'
					value={email}
					onChange={(e) => setEmail(e.target.value)}
				/>

				<label htmlFor='password'>Password:</label>
				<Input
					id='password'
					type='password'
					placeholder='Password'
					value={password}
					onChange={(e) => setPassword(e.target.value)}
				/>

				<Button buttonText='Register' />
			</form>
		</div>
	);
}

export default Registration;
