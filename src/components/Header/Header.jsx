import Logo from './components/Logo/logo';
import Button from '../../common/Button/button';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { logoutUser as logoutUserAction } from '../../store/user/actionCreators';
import { logoutUser } from '../../services';
import './Header.css';

function Header() {
	const user = useSelector((state) => state.user);
	const isAuthenticated = useSelector((state) => state.user.isAuthenticated);
	const dispatch = useDispatch();
	const navigate = useNavigate();

	const handleLogout = async () => {
		try {
			await logoutUser(user.token);
			dispatch(logoutUserAction());
			localStorage.removeItem('token');
			navigate('/login');
		} catch (error) {
			console.error('Error logging out user:', error);
		}
	};

	return (
		<header className='header-container'>
			<Logo />
			{isAuthenticated && (
				<div className='header-text-button-container'>
					<span className='header-text'>{user.name}</span>
					<Button buttonText='Logout' onClick={handleLogout} />
				</div>
			)}
		</header>
	);
}

export default Header;
