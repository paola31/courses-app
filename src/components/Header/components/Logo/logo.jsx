import logo512 from '../../../../assets/images/logo512.png';

function Logo() {
	return <img src={logo512} alt={'Logo'} width={60} height={60} />;
}

export default Logo;
