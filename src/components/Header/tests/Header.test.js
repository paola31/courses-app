import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import Header from '../Header';
import { BrowserRouter as Router } from 'react-router-dom';

const mockedState = {
	user: {
		isAuth: true,
		isAuthenticated: true,
		name: 'Test Name',
	},
};

const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

describe('Header Component', () => {
	beforeEach(() => {
		render(
			<Provider store={mockedStore}>
				<Router>
					<Header />
				</Router>
			</Provider>
		);
	});

	test('Header should have logo', () => {
		expect(screen.getByAltText('Logo')).toBeInTheDocument();
	});

	test("Header should display user's name", () => {
		expect(screen.getByText(mockedState.user.name)).toBeInTheDocument();
	});
});
