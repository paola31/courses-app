import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import CourseCard from '../CourseCard';
import { BrowserRouter as Router } from 'react-router-dom';

describe('CourseCard Component', () => {
	const mockedCourse = {
		id: 1,
		title: 'Course Title',
		description: 'Course Description',
		duration: 90,
		authors: ['1', '2'],
		creationDate: '2023-05-09',
	};

	const mockedAuthors = [
		{ id: '1', name: 'Author 1' },
		{ id: '2', name: 'Author 2' },
	];

	const mockedDelete = jest.fn();

	const mockedState = {
		user: {
			isAuth: true,
			isAuthenticated: true,
			name: 'Test Name',
		},
	};

	const mockedStore = {
		getState: () => mockedState,
		subscribe: jest.fn(),
		dispatch: jest.fn(),
	};

	beforeEach(() => {
		render(
			<Provider store={mockedStore}>
				<Router>
					<CourseCard
						course={mockedCourse}
						authors={mockedAuthors}
						onDelete={mockedDelete}
					/>
				</Router>
			</Provider>
		);
	});

	test('CourseCard should display title', () => {
		expect(screen.getByText(mockedCourse.title)).toBeInTheDocument();
	});

	test('CourseCard should display description', () => {
		expect(screen.getByText(mockedCourse.description)).toBeInTheDocument();
	});

	test('CourseCard should display duration in the correct format', () => {
		expect(screen.getByText('Duration: 1:30 hours')).toBeInTheDocument();
	});

	test('CourseCard should display authors list', () => {
		expect(screen.getByText('Authors: Author 1, Author 2')).toBeInTheDocument();
	});

	test('CourseCard should display created date', () => {
		expect(
			screen.getByText(`Created: ${mockedCourse.creationDate}`)
		).toBeInTheDocument();
	});
});
