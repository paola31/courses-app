import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Button from '../../../../common/Button/button';
import './CourseCard.css';

function CourseCard({ course, authors, onDelete }) {
	const user = useSelector((state) => state.user);
	const formatDuration = (duration) => {
		const hours = Math.floor(duration / 60);
		const minutes = duration % 60;
		return `${hours}:${minutes.toString().padStart(2, '0')} hours`;
	};

	const getAuthors = (authorIds, authors) => {
		return authorIds
			.map((authorId) => {
				const author = authors.find((author) => author.id === authorId);
				return author ? author.name : '';
			})
			.join(', ');
	};

	const navigate = useNavigate();

	const handleShowCourse = () => {
		if (user.role === 'admin') {
			navigate(`/courses/update/${course.id}`, {
				state: { course, authors },
			});
		} else {
			navigate(`/courses/${course.id}`);
		}
	};

	const handleDeleteCourse = () => {
		onDelete(course.id);
	};

	return (
		<div className='course-card'>
			<div className='course-info-left'>
				<h3 className='course-title'>{course.title}</h3>
				<p className='course-description'>{course.description}</p>
			</div>
			<div className='course-info-right'>
				<p className='course-authors'>
					Authors: {authors && getAuthors(course.authors, authors)}
				</p>
				<p className='course-duration'>
					Duration: {formatDuration(course.duration)}
				</p>
				<p className='course-date'>Created: {course.creationDate}</p>
				<Button buttonText='Show Course' onClick={() => handleShowCourse()} />
				{user.role === 'admin' && (
					<div>
						<Button buttonText='Delete' onClick={handleDeleteCourse} />
						<Button buttonText='Update' />
					</div>
				)}
			</div>
		</div>
	);
}

export default CourseCard;
