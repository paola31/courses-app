import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchCourses } from '../../store/courses/actionCreators';
import { fetchAuthors } from '../../store/authors/actionCreators';
import { deleteCourseThunk } from '../../store/courses/thunk';
import SearchBar from './components/SearchBar/SearchBar';
import CourseCard from './components/CourseCard/CourseCard';
import Button from '../../common/Button/button';
import { useNavigate } from 'react-router-dom';
import './Courses.css';

function Courses() {
	const [searchQuery, setSearchQuery] = useState('');
	const [filteredCourses, setFilteredCourses] = useState([]);
	const courses = useSelector((state) => state.courses);
	const authors = useSelector((state) => state.authors);

	const dispatch = useDispatch();
	const navigate = useNavigate();

	useEffect(() => {
		if (courses.length === 0) {
			dispatch(fetchCourses());
		}
		if (authors.length === 0) {
			dispatch(fetchAuthors());
		}
	}, [authors.length, courses.length, dispatch]);

	useEffect(() => {
		setFilteredCourses(courses);
	}, [courses]);

	const handleSearch = (query) => {
		console.log(searchQuery);
		setSearchQuery(query);
		if (query) {
			setFilteredCourses(
				courses.filter(
					(course) =>
						course.title.toLowerCase().includes(query.toLowerCase()) ||
						course.id.toLowerCase().includes(query.toLowerCase())
				)
			);
		} else {
			setFilteredCourses(courses);
		}
	};

	const handleAddNewCourse = () => {
		navigate('/courses/add');
	};

	const handleDeleteCourse = (courseId) => {
		dispatch(deleteCourseThunk(courseId));
	};

	return (
		<div className='courses'>
			<div className='courses-top'>
				<SearchBar onSearch={handleSearch} />
				<Button buttonText='Add New Course' onClick={handleAddNewCourse} />
			</div>
			<div className='courses-list'>
				{filteredCourses
					.filter((course) => course !== null && course !== undefined)
					.map((course) => (
						<CourseCard
							key={course.id}
							course={course}
							authors={authors || []}
							onDelete={handleDeleteCourse}
						/>
					))}
			</div>
		</div>
	);
}

export default Courses;
