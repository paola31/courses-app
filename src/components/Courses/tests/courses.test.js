import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import Courses from '../Courses';
import { BrowserRouter as Router } from 'react-router-dom';

describe('Courses Component', () => {
	const mockedCourses = [
		{
			id: '1',
			title: 'Course 1',
			description: 'Course 1 description',
			duration: 60,
			authors: ['1'],
			creationDate: '9/14/2021',
		},
		{
			id: '2',
			title: 'Course 2',
			description: 'Course 2 description',
			duration: 120,
			authors: ['2'],
			creationDate: '9/15/2021',
		},
	];

	const mockedAuthors = [
		{ id: '1', name: 'Author 1' },
		{ id: '2', name: 'Author 2' },
	];

	const mockedState = {
		user: {
			isAuth: true,
			isAuthenticated: true,
			name: 'Test Name',
			role: 'admin',
		},
		courses: mockedCourses,
		authors: mockedAuthors,
	};

	const mockedStore = {
		getState: () => mockedState,
		subscribe: jest.fn(),
		dispatch: jest.fn(),
	};

	test('Courses should display amount of CourseCard equal length of courses array', () => {
		render(
			<Provider store={mockedStore}>
				<Router>
					<Courses />
				</Router>
			</Provider>
		);
		const courseCards = document.querySelectorAll('.course-card');
		expect(courseCards.length).toEqual(mockedCourses.length);
	});

	test('Courses should display Empty container if courses array length is 0', () => {
		mockedState.courses = [];
		render(
			<Provider store={mockedStore}>
				<Router>
					<Courses />
				</Router>
			</Provider>
		);
		const courseCards = document.querySelectorAll('.course-card');
		expect(courseCards.length).toEqual(0);
	});

	/*    test('CourseForm should be showed after a click on a button "Add new course"', () => {
        render(
            <Provider store={mockedStore}>
                <Router>
                    <Courses />
                </Router>
            </Provider>
        );
        fireEvent.click(document.querySelector('.ADC'));
        expect( document.querySelector('.create-course')).toBeInTheDocument();
    });*/
});
