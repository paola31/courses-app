import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { login } from '../../store/user/thunk';
import Input from '../../common/Input/Input';
import Button from '../../common/Button/button';
import './Login.css';

function Login() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const navigate = useNavigate();
	const dispatch = useDispatch();

	const handleSubmit = async (e) => {
		e.preventDefault();
		try {
			await dispatch(login(email, password));
			navigate('/courses');
		} catch (error) {
			console.error(error);
		}
	};

	return (
		<div className='login'>
			<h2>Login</h2>
			<form onSubmit={handleSubmit}>
				<label htmlFor='email'>Email</label>
				<Input
					type='email'
					id='email'
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>
				<label htmlFor='password'>Password</label>
				<Input
					type='password'
					id='password'
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					required
				/>
				<Button buttonText='Login' type='submit' />
			</form>
			<p>
				Don't have an account? <Link to='/registration'>Register</Link>
			</p>
		</div>
	);
}

export default Login;
