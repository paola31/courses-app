import React from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import Button from '../../common/Button/button';
import './CourseInfo.css';

function CourseInfo() {
	const location = useLocation();
	const { course, authors } = location.state || { course: null, authors: null };
	const navigate = useNavigate();

	const handleBackToCourses = () => {
		navigate('/courses');
	};

	const getAuthorNames = (authorIds) => {
		return authorIds
			.map((authorId) => {
				const author = authors.find((author) => author.id === authorId);
				return author ? author.name : 'Unknown';
			})
			.join(', ');
	};

	if (!course) {
		return (
			<div>
				Course not found
				<Button onClick={handleBackToCourses}>Back to courses</Button>
			</div>
		);
	}

	return (
		<div className='course-info'>
			<h2>{course.title}</h2>
			<p>ID: {course.id}</p>
			<p>Description: {course.description}</p>
			<p>Duration: {course.duration}</p>
			<p>Authors: {getAuthorNames(course.authors)}</p>
			<p>Creation date: {course.creationDate}</p>
			<Button buttonText='Back to courses' onClick={handleBackToCourses} />
		</div>
	);
}

export default CourseInfo;
