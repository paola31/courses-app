import { Navigate, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';

function PrivateRoute({ children, roles }) {
	const location = useLocation();
	const { role } = useSelector((state) => state.user);

	if (roles.includes(role)) {
		return children;
	} else {
		return <Navigate to='/courses' replace state={{ from: location }} />;
	}
}

export default PrivateRoute;
