import React from 'react';
import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import Login from './components/Login/Login';
import CourseForm from './components/CourseForm/CourseForm';
import Registration from './components/Registration/Registration';
import CourseInfo from './components/CourseInfo/CourseInfo';
import PrivateRoute from './components/PrivateRouter/PrivateRouter';

import {
	BrowserRouter as Router,
	Route,
	Routes,
	Navigate,
	useLocation,
} from 'react-router-dom';
import './App.css';

const token = localStorage.getItem('token');
const defaultPath = token ? '/courses' : '/login';

function AppContent() {
	const location = useLocation();
	const showHeader =
		location.pathname === '/courses' ||
		location.pathname === '/courses/add' ||
		location.pathname.startsWith('/courses/update');

	return (
		<div className='app-container'>
			{showHeader && <Header />}
			<Routes>
				<Route path='/courses' element={<Courses />} />
				<Route path='/courses/:courseId' element={<CourseInfo />} />
				<Route
					path='/courses/add'
					element={
						<PrivateRoute roles={['admin']}>
							<CourseForm />
						</PrivateRoute>
					}
				/>
				<Route
					path='/courses/update/:courseId'
					element={
						<PrivateRoute roles={['admin']}>
							<CourseForm />
						</PrivateRoute>
					}
				/>
				<Route path='/registration' element={<Registration />} />
				<Route path='/login' element={<Login />} />
				<Route path='/' element={<Navigate to={defaultPath} />} />
			</Routes>
		</div>
	);
}

function App() {
	return (
		<Router>
			<AppContent />
		</Router>
	);
}

export default App;
