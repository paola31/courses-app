import axios from 'axios';

const API_BASE_URL = 'http://localhost:4000';

export async function fetchCoursesFromBack() {
	try {
		const response = await axios.get(`${API_BASE_URL}/courses/all`);
		return response.data;
	} catch (error) {
		console.error('Error fetching courses:', error);
		throw error;
	}
}

export async function fetchAuthorsFromBack() {
	try {
		const response = await axios.get(`${API_BASE_URL}/authors/all`);
		return response.data;
	} catch (error) {
		console.error('Error fetching authors:', error);
		throw error;
	}
}

export const login = async (email, password) => {
	try {
		const response = await axios.post(`${API_BASE_URL}/login`, {
			email,
			password,
		});

		const { result, user } = response.data;

		return {
			id: user.email,
			name: user.name,
			token: result,
		};
	} catch (error) {
		throw error;
	}
};

export const fetchCurrentUser = async (token) => {
	try {
		const response = await axios.get(`${API_BASE_URL}/users/me`, {
			headers: {
				Authorization: token,
			},
		});
		return response.data;
	} catch (error) {
		console.error('Error fetching current user:', error);
		throw error;
	}
};

export const updateCourse = async (courseId, courseData) => {
	try {
		const response = await axios.put(
			`${API_BASE_URL}/courses/${courseId}`,
			courseData
		);
		return response.data;
	} catch (error) {
		throw error;
	}
};

export const logoutUser = async (token) => {
	try {
		console.log('Tokenb logout: ' + token);
		const response = await axios.delete(`${API_BASE_URL}/logout`, {
			headers: {
				Authorization: token,
			},
		});
		return response.data;
	} catch (error) {
		console.error('Error logging out user:', error);
		throw error;
	}
};

export const deleteCourseFromApi = async (courseId) => {
	const token = localStorage.getItem('token');
	const url = `${API_BASE_URL}/courses/${courseId}`;

	const requestOptions = {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `${token}`,
		},
	};

	try {
		const response = await fetch(url, requestOptions);
		if (response.ok) {
			return { successful: true };
		} else {
			const errorData = await response.json();
			return { successful: false, error: errorData };
		}
	} catch (error) {
		console.error('Error while deleting course:', error);
		return { successful: false, error };
	}
};

export const addCourseToApi = async (course) => {
	const token = localStorage.getItem('token');
	const url = `${API_BASE_URL}/courses/add`;

	const requestOptions = {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `${token}`,
		},
		body: JSON.stringify(course),
	};

	try {
		const response = await fetch(url, requestOptions);
		if (response.ok) {
			const newCourse = await response.json();
			return { successful: true, course: newCourse };
		} else {
			const errorData = await response.json();
			return { successful: false, error: errorData };
		}
	} catch (error) {
		console.error('Error while adding course:', error);
		return { successful: false, error };
	}
};

export const updateCourseInApi = async (courseId, course) => {
	const token = localStorage.getItem('token');
	const url = `${API_BASE_URL}/courses/${courseId}`;

	const requestOptions = {
		headers: {
			'Content-Type': 'application/json',
			Authorization: `${token}`,
		},
	};

	try {
		const response = await axios.put(url, course, requestOptions);
		const updatedCourse = response.data;
		return { successful: true, course: updatedCourse };
	} catch (error) {
		console.error('Error while updating course:', error);
		return { successful: false, error: error.response.data };
	}
};

export const addAuthor = async (authorData) => {
	try {
		const token = localStorage.getItem('token');
		const response = await axios.post(
			`${API_BASE_URL}/authors/add`,
			authorData,
			{
				headers: {
					Authorization: token ? `${token}` : null,
				},
			}
		);
		return response.data;
	} catch (error) {
		throw error;
	}
};
