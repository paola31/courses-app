function Input({
	placeholder,
	onChange,
	id,
	labelText = '',
	type = 'text',
	value = '',
}) {
	return (
		<div>
			<label htmlFor={id}>{labelText}</label>
			<input
				type={type}
				value={value}
				placeholder={placeholder}
				onChange={onChange}
				id={id}
			/>
		</div>
	);
}

export default Input;
