import reducer from '../reducer';
import {
	ADD_COURSE,
	UPDATE_COURSE,
	FETCH_COURSES,
	DELETE_COURSE,
} from '../actionTypes';

describe('Courses Reducer', () => {
	const initialState = [];

	it('should return the initial state', () => {
		expect(reducer(undefined, {})).toEqual(initialState);
	});

	it('should handle FETCH_COURSES and return new state', () => {
		const courses = [
			{ id: 1, title: 'Course 1' },
			{ id: 2, title: 'Course 2' },
		];
		const action = { type: FETCH_COURSES, payload: courses };
		expect(reducer(initialState, action)).toEqual(courses);
	});

	it('should handle ADD_COURSE and return new state', () => {
		const course = { id: 3, title: 'Course 3' };
		const action = { type: ADD_COURSE, payload: course };
		const expectedState = [...initialState, course];
		expect(reducer(initialState, action)).toEqual(expectedState);
	});

	it('should handle DELETE_COURSE and return new state', () => {
		const courseToDelete = 1;
		const action = { type: DELETE_COURSE, payload: courseToDelete };
		const expectedState = initialState.filter(
			(course) => course.id !== courseToDelete
		);
		expect(reducer(initialState, action)).toEqual(expectedState);
	});

	it('should handle UPDATE_COURSE and return new state', () => {
		const updatedCourse = { id: 1, title: 'Updated Course 1' };
		const action = { type: UPDATE_COURSE, payload: updatedCourse };
		const expectedState = initialState.map((course) =>
			course.id === updatedCourse.id ? updatedCourse : course
		);
		expect(reducer(initialState, action)).toEqual(expectedState);
	});
});
