import {
	ADD_COURSE,
	UPDATE_COURSE,
	DELETE_COURSE,
	FETCH_COURSES,
} from './actionTypes';

const initialState = [];

const coursesReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_COURSES:
			return action.payload;
		case ADD_COURSE:
			return [...state, action.payload];
		case DELETE_COURSE:
			return state.filter((course) => course.id !== action.payload);
		case UPDATE_COURSE:
			return state.map((course) =>
				course.id === action.payload.id ? action.payload : course
			);
		default:
			return state;
	}
};

export default coursesReducer;
