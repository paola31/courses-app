import {
	deleteCourseFromApi,
	addCourseToApi,
	updateCourseInApi,
} from '../../services';
import { deleteCourse, addCourse, updateCourse } from './actionCreators';

export const deleteCourseThunk = (courseId) => async (dispatch) => {
	try {
		const response = await deleteCourseFromApi(courseId);
		if (response.successful) {
			dispatch(deleteCourse(courseId));
		} else {
			console.error('Error deleting course');
		}
	} catch (error) {
		console.error(error);
	}
};

export const addCourseThunk = (course) => async (dispatch) => {
	try {
		const response = await addCourseToApi(course);
		if (response.successful) {
			dispatch(addCourse(response.result));
		} else {
			console.log('Failed to add a new course');
		}
	} catch (error) {
		console.error(error);
	}
};

export const updateCourseThunk = (courseId, course) => async (dispatch) => {
	try {
		const response = await updateCourseInApi(courseId, course);
		if (response.successful) {
			dispatch(updateCourse(response.course));
		} else {
			console.log('Failed to update a course: ' + JSON.stringify(response));
		}
	} catch (error) {
		console.error(error);
	}
};
