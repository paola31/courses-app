import {
	GET_COURSES,
	ADD_COURSE,
	UPDATE_COURSE,
	DELETE_COURSE,
	FETCH_COURSES,
} from './actionTypes';

import { fetchCoursesFromBack } from '../../services';

export const getCourses = (courses) => ({
	type: GET_COURSES,
	payload: courses,
});

export const addCourse = (course) => ({
	type: ADD_COURSE,
	payload: course,
});

export const updateCourse = (course) => ({
	type: UPDATE_COURSE,
	payload: course,
});

export const deleteCourse = (courseId) => ({
	type: DELETE_COURSE,
	payload: courseId,
});

export const fetchCourses = () => {
	return async (dispatch) => {
		try {
			const response = await fetchCoursesFromBack();
			const courses = response.result;

			dispatch({
				type: FETCH_COURSES,
				payload: courses,
			});
		} catch (error) {
			console.error('Error fetching courses:', error);
		}
	};
};
