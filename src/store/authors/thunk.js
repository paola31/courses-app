import { addAuthor } from './actionCreators';
import { addAuthor as addAuthorAPI } from '../../services';

export const addAuthorThunk = (author) => {
	return async (dispatch) => {
		try {
			const savedAuthor = await addAuthorAPI(author);
			console.log('Saved author:', savedAuthor);
			if (savedAuthor) {
				dispatch(addAuthor(savedAuthor.result));
			} else {
				alert('An error occurred while saving the author. Please try again.');
			}
		} catch (error) {
			console.error('Error creating author:', error);
			alert('An error occurred while saving the author. Please try again.');
		}
	};
};
