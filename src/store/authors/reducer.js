import { ADD_AUTHOR, FETCH_AUTHORS } from './actionTypes';
const initialState = [];

const authorsReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_AUTHORS:
			return action.payload;
		case ADD_AUTHOR:
			return [...state, action.payload];
		default:
			return state;
	}
};

export default authorsReducer;
