import { GET_AUTHORS, ADD_AUTHOR, FETCH_AUTHORS } from './actionTypes';
import { fetchAuthorsFromBack } from '../../services';

export const getAuthors = (authors) => ({
	type: GET_AUTHORS,
	payload: authors,
});

export const addAuthor = (author) => ({
	type: ADD_AUTHOR,
	payload: author,
});

export const fetchAuthors = () => {
	return async (dispatch) => {
		try {
			const response = await fetchAuthorsFromBack();
			dispatch({
				type: FETCH_AUTHORS,
				payload: response.result,
			});
		} catch (error) {
			console.error('Error fetching authors:', error);
		}
	};
};
