import { LOGIN_USER, LOGOUT_USER, SET_CURRENT_USER } from './actionTypes';

export const loginUser = (user) => ({
	type: LOGIN_USER,
	payload: user,
});

export const setCurrentUser = (user) => ({
	type: SET_CURRENT_USER,
	payload: user,
});

export const logoutUser = () => ({
	type: LOGOUT_USER,
});
