import { loginUser, setCurrentUser } from './actionCreators';
import {
	login as loginApi,
	fetchCurrentUser as fetchCurrentUserApi,
} from '../../services';

export const fetchCurrentUser = () => async (dispatch) => {
	const token = localStorage.getItem('token');

	if (token) {
		try {
			const userData = await fetchCurrentUserApi(token);
			dispatch(setCurrentUser(userData));
		} catch (error) {
			console.error('Error fetching current user:', error);
		}
	}
};

export const login = (email, password) => async (dispatch) => {
	try {
		const userData = await loginApi(email, password);
		localStorage.setItem('token', userData.token);
		dispatch(loginUser(userData));

		const currentUser = await fetchCurrentUserApi(userData.token);
		dispatch(setCurrentUser(currentUser.result));

		return userData;
	} catch (error) {
		console.error(error);
		throw error;
	}
};
